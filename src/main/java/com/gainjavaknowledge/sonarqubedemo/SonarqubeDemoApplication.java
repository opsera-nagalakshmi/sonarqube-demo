package com.gainjavaknowledge.sonarqubedemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SonarqubeDemoApplication {

	public static void main(String[] args) {
        String password = "my_secret_password";
        System.out.println("Password: " + password);
		SpringApplication.run(SonarqubeDemoApplication.class, args);
        String input = null;
        int length = input.length(); // Potential null pointer dereference
        System.out.println("Length: " + length);
	}
	
    public int add(int a, int b) {
        return a + b;
    }
    public int subtract(int a, int b) {
        return a - b;
    }
    public int divide(int a, int b) {
        return a / b;
    }
    public void executeQuery(String userInput) {
        String query = "SELECT * FROM users WHERE username = '" + userInput + "'";
        Statement statement = connection.createStatement();
        statement.executeQuery(query);
    }


}
